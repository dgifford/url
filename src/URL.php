<?php
Namespace dgifford\URL;



/*
	Class for manipulating paths and files.
 */



Use GuzzleHttp\Client;



class URL
{
	// Array of URLs
	public $urls = [];

	protected $parseurl_options = 
	[
		'scheme' 	=> 'PHP_URL_SCHEME',
		'host' 		=> 'PHP_URL_HOST',
		'port' 		=> 'PHP_URL_PORT',
		'user' 		=> 'PHP_URL_USER',
		'pass' 		=> 'PHP_URL_PASS',
		'path' 		=> 'PHP_URL_PATH',
		'query' 	=> 'PHP_URL_QUERY',
		'frgament' 	=> 'PHP_URL_FRAGMENT',
	];

	protected $exists_timeout = 10;






	public function __construct()
	{
		call_user_func_array([ $this, 'set' ], func_get_args());
	}



	public function set()
	{
		$result = [];

		$args = func_get_args();

		// Flatten the array
		array_walk_recursive($args, function($a) use (&$result) { $result[] = $a; });
	    
	    $this->urls =  $result;

		// Chainable
		return $this;
	}



	public function get()
	{
		if( $this->count() == 1 )
		{
			return $this->urls[0];
		}

		return $this->urls;
	}






	///////////////////////////////////////////////
	// static_shared methods are shared between
	// static and non-static calls
	///////////////////////////////////////////////



	protected function count_static_shared()
	{
		return count( $this->urls );
	}



	/**
	 * Returns false if any of the URL(s) are invalid.
	 * Set $each to true to return an array of results if multiple
	 * URLs are provided.
	 * 
	 * @return bool/ array
	 */
	protected function valid_static_shared( $each = false )
	{
		$result = true;

		if( $each )
		{
			$result = [];
		}

		foreach( $this->urls as $url )
		{
			if( !is_string( $url ) or !$this->validFormat( $url ) )
			{
				if( $each )
				{
					$result[$url] = false;
				}
				else
				{
					$result = false;
				}			
			}
			elseif( $each )
			{
				$result[$url] = true;
			}
		}

		return $result;
	}



	public function validFormat( $url = '' )
	{
		if( filter_var( $url, FILTER_VALIDATE_URL ) === false )
		{
			return false;
		}

		return true;
	}



	/**
	 * Joins parts of a URL together.
	 * 
	 * @return [type] [description]
	 */
	public function join_static_shared()
	{
		$parts = [];

		// Merge sub arrays
		foreach( $this->urls as $part )
		{
			if(is_array($part))
			{
				$parts = array_merge($parts, $part);
			}
			else
			{
				$parts[] = $part;
			}
		}

		$scheme = '';

		if( substr($parts[0], -2) == '//' or substr($parts[0], -1) == ':' )
		{
			$scheme = $parts[0];
			unset( $parts[0] );
		}

		foreach( $parts as &$part )
		{
			$part = rtrim( $part, '\\/');
		}

		$this->set( $scheme . implode( '/', $parts) );

		// Chainable
		return $this;
	}



	/**
	 * Treats the items in urls as the parts of a URL, joins them together
	 * and returns a boolean result if the resulting URL exists
	 * 
	 * @return [type] [description]
	 */
	protected function joinExists_static_shared()
	{
		$this->join();

		return $this->exists();
	}




	/**
	 * Returns false if any of the URL(s) don't exist.
	 * @return bool/ array
	 */
	protected function exists_static_shared( $each = false )
	{
		$result = true;

		if( $each )
		{
			$result = [];
		}

		foreach( $this->urls as $url )
		{
			if( !is_string( $url ) or !$this->canFetchHead( $url ) )
			{
				if( $each )
				{
					$result[$url] = false;
				}
				else
				{
					$result = false;
				}			
			}
			elseif( $each )
			{
				$result[$url] = true;
			}
		}

		return $result;
	}



	/**
	 * Return false if a HEAD request to the URL fails.
	 * 
	 * @param  string $url [description]
	 * @return bool
	 */
	public function canFetchHead( $url = '' )
	{
		$client = new Client([ 'timeout' => $this->exists_timeout ]);

		try
		{
			$client->head( $url );
			return true;
		}
		catch( \GuzzleHttp\Exception\ConnectException $e ) {}
		catch( \GuzzleHttp\Exception\ClientException $e) {}

		return false;
	}



	/**
	 * Returns false if any of the URL(s) aren't writable.
	 * @return [type] [description]
	 */
	protected function writeable_static_shared()
	{
		foreach( $this->urls as $url )
		{
			if( !is_string( $url ) or !is_writable( $url ) )
			{
				return false;
			}
		}

		return true;
	}



	/**
	 * Gets info about the URL(s)
	 * @param  string $option [description]
	 * @return [type]         [description]
	 */
	protected function info_static_shared( $option = '' )
	{
		if( isset( $this->parseurl_options[ $option ]) )
		{
			$option = $this->parseurl_options[ $option ];
		}
		elseif( !in_array( $option, $this->parseurl_options ))
		{
			$option = '';
		}

		$result = [];

		foreach( $this->urls as $url )
		{
			if( !empty($option) )
			{
				$result[$url] = parse_url( $url, constant( $option ) );
			}
			else
			{
				$result[$url] = parse_url( $url );
			}
			
		}

		if( count($result) == 1 )
		{
			return array_pop( $result );
		}

		return $result;
	}



	protected function scheme_static_shared()
	{
		return $this->info( 'scheme' );
	}



	public function host_static_shared()
	{
		return $this->info( 'host' );
	}



	protected function port_static_shared()
	{
		return $this->info( 'port' );
	}



	protected function user_static_shared()
	{
		return $this->info( 'user' );
	}



	protected function pass_static_shared()
	{
		return $this->info( 'pass' );
	}



	protected function path_static_shared()
	{
		return $this->info( 'path' );
	}



	protected function query_static_shared()
	{
		return $this->info( 'query' );
	}



	protected function fragment_static_shared()
	{
		return $this->info( 'fragment' );
	}






	////////////////////////////////////////////
	// Special Static methods
	////////////////////////////////////////////



	public static function eachExists( $arg )
	{
		$url = new URL( $arg );

		return $url->exists( true );
	}




	////////////////////////////////////////////
	// Overloaders - allow the use of the same 
	// method names for static and non-static
	////////////////////////////////////////////



	public function __call( $name, $args = [] )
	{
		$name = URL::getMethodName( $name );

		if( !empty( $args ) )
		{
			return call_user_func_array([ $this, $name ], $args );
		}
		else
		{
			return $this->$name();
		}
	}



	/**
	 * When called statically, multiple paths must be
	 * contained in an array.
	 * 
	 * @param  [type] $name [description]
	 * @param  [type] $args [description]
	 * @return [type]       [description]
	 */
	public static function __callStatic( $name, $args )
	{
		$name = URL::getMethodName( $name );

		if( isset( $args[0] ) )
		{
			$url = new URL( $args[0] );

			if( isset( $args[1] ) )
			{
				$result = $url->$name( $args[1] );
			}
			else
			{
				$result = $url->$name();
			}

			if( $result instanceof URL )
			{
				return $result->get();
			}
			else
			{
				return $result;
			}
		}
	}


	protected static function getMethodName( $name = '' )
	{
		if( !method_exists( (new URL), strtolower( $name ) . '_static_shared' ) )
		{
			throw new \BadMethodCallException("Method '{$name}' does not exist." );			
		}

		return strtolower( $name ) . '_static_shared';
	}
}