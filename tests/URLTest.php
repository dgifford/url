<?php
Namespace dgifford\URL;



/**
 * Auto Loader
 * 
 */
require_once(__DIR__ . '/../vendor/autoload.php');



class URLTest extends \PHPUnit_Framework_TestCase
{
	public function testConstructor()
	{
		$url = new URL( 'https://google.com' );

		$this->assertTrue( $url->valid() );

		$this->assertTrue( $url->exists() );

		$this->assertSame( 1, $url->count() );
	}


	public function testSet()
	{
		$url = new URL;

		$url->set( 'https://google.com' );

		$this->assertTrue( $url->valid() );

		$this->assertTrue( $url->exists() );

		$this->assertSame( 1, $url->count() );
	}



	public function testMultipleURLs()
	{
		$url = new URL( 'https://google.com', 'http://bbc.co.uk', 'http://php.net' );

		$this->assertSame( ['https://google.com', 'http://bbc.co.uk', 'http://php.net'], $url->get() );

		$url->set(['https://google.com', 'http://bbc.co.uk', 'http://php.net']);

		$this->assertSame( ['https://google.com', 'http://bbc.co.uk', 'http://php.net'], $url->get() );

		$url->set(['https://google.com', 'http://bbc.co.uk', ], 'http://php.net');

		$this->assertSame( ['https://google.com', 'http://bbc.co.uk', 'http://php.net'], $url->get() );

		$url->set(['https://google.com']);

		$this->assertSame( 'https://google.com', $url->get() );
	}



	public function testURLExists()
	{
		$url = new URL( 'https://sdfjsdfjksdfjksdfjk.sdf' );

		$this->assertFalse( $url->exists() );

		$url->set( 'https://google.com' );

		$this->assertTrue( $url->exists() );

		$url->set( 'https://google.com', 'https://sdfjsdfjksdfjksdfjk.sdf', 'https://sdfjsdgdfgdfjksdfjksdfjk.sdf' );

		$this->assertFalse( $url->exists() );

		$this->assertSame([ 'https://google.com' => true, 'https://sdfjsdfjksdfjksdfjk.sdf' => false, 'https://sdfjsdgdfgdfjksdfjksdfjk.sdf' => false], $url->exists( true ) );
	}



	public function testJoinURLs()
	{
		$url = new URL( 'http://', 'google.com', 'gfgsdgfsdgdfs' );

		$this->assertSame( 'http://google.com/gfgsdgfsdgdfs', $url->join()->get() );

		$this->assertFalse( $url->joinExists() );

		$url->set( 'http://php.net', 'manual', 'en', 'function.parse-url.php' );

		$this->assertTrue( $url->joinExists() );
	}



	public function testInfo()
	{
		$url = new URL( 'http://php.net/manual/en/function.parse-url.php' );

		$this->assertTrue( $url->exists() );

		$this->assertSame( 'http', $url->scheme() );
		
		$this->assertSame( '/manual/en/function.parse-url.php', $url->path() );
	}



	public function testStaticMethods()
	{
		$this->assertTrue( URL::exists( 'http://google.com' ) );

		$this->assertSame( 1, URL::count( 'http://google.com' ) );

		$this->assertTrue( URL::valid( 'http://google.com' ) );

		$this->assertTrue( URL::valid([ 'http://google.com', 'http://bbc.co.uk', 'http://php.net' ]) );

		$this->assertSame( 'http://php.net/manual/en/function.parse-url.php', URL::join([ 'http://php.net', 'manual', 'en/function.parse-url.php' ]) );

		$this->assertTrue( URL::exists([ 'http://google.com', 'http://bbc.co.uk', 'http://php.net' ]) );

		$this->assertSame( [ 'http://google.com' => true, 'http://bbc.co.uk' => true, 'http://php.net' => true ], URL::eachExists([ 'http://google.com', 'http://bbc.co.uk', 'http://php.net' ]) );

		$this->assertSame( 'google.com', URL::host( 'https://google.com' ) );

		$this->assertSame( 's=wibble', URL::query( 'https://google.com?s=wibble' ) );

	}



    /**
     * @expectedException     \BadMethodCallException
     */
	public function testMethodDoesntExist()
	{
		$url = new URL;

		$url->foobar();
	}



    /**
     * @expectedException     \BadMethodCallException
     */
	public function testStaticMethodDoesntExist()
	{
		URL::foobar('wibble');
	}
}